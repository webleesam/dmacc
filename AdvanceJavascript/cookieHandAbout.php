<?php
/*
Provide a link on the home page to the about page.  
Use a persistent (for 3 days) cookie to count the number of times the home page has been accessed.  Display the results on the about page. 
Use a session cookie to store what website the user was viewing before coming to the home page.  Display the result on the about page. The value of this cookie should only be set once when entering the home page. 
Create a button on the home page that will create a secured persistent (6 months) authentication cookie. 
The about page will check for the authentication cookie, if available display the about page, otherwise return to the home page. 
Create a button on the about page that will destroy all of the cookies so you can test this application.   
Present your project to the class.  Discuss how your application works. 
*/
?>


<html>
<head>
 <title>Cookie About</title>

<script language="javascript">

// This is a 'generic' function that will add a value or cookie to the document
// It uses name-value pairs as input parameters to the function
// 'tag' is the name of the data that you wish to store
// 'value' contains the contents that are associated with the name of the data

  function addCookie(tag, value) {
    var expireDate = new Date()
    var expireString = ""
    expireDate.setTime(expireDate.getTime() + (1000 * 60 * 60 * 24 * 365) )
    expireString = "expires="+ expireDate.toGMTString()
    document.cookie = tag + "=" + escape(value) + ";" + expireString + ";"
  }


// This is a 'generic' function that will look for a specific piece of information 
// in a cookie and return its value.  
// The 'name' of the function is passed to the function using the 'tag' parameter
// 'tag' contains the name of the name-value pair that you wish to find
// This function will return the value associated with the name requested.

  function getCookie(tag) {
    var value = null
    var myCookie = document.cookie + ";"
    var findTag = tag + "="
    var endPos
    if (myCookie.length > 0 ) {
      var beginPos = myCookie.indexOf(findTag)
      if (beginPos != -1) {
        beginPos = beginPos + findTag.length
        endPos = myCookie.indexOf(";", beginPos)
        if (endPos == -1)
          endPos = myCookie.length
        value = unescape(myCookie.substring(beginPos, endPos))
      }
    } 
   return value   
  } 


// This is a 'generic' function tht will delete the cookie.  This is done by setting 
// the expiration date of the cookie to yesterday.
// 'tag' contains the name of the cookie element that you wish to delete.

  function deleteCookie(tag) {
    var Yesterday = 24 * 60 * 60 * 1000
    var expireDate = new Date()
    expireDate.setTime (expireDate.getTime() - Yesterday)
    document.cookie = tag + "=nothing; expires=" + expireDate.toGMTString()

//  Example statements
//   document.cookie = "StudentName=nothing; expires="+ expireDate.toGMTString()
//   document.cookie = "HitCounter=nothing; expires="+ expireDate.toGMTString()

  }

// This function will display the contents of the entire cookie

  function displayCookie() {

    alert("Contents of Cookie: " + document.cookie)

  }

//END OF COOKIE FUNCTIONS

//BEGIN regular functions

  function getUserName() {

    //get username from prompt window
    var strName = window.prompt("Hello, What is your name?", "");

    //create cookie element named 'UserName' with a value stored in strName
    addCookie("UserName",strName);

  }


  function displayUserName () {

    //display the UserName element of the cookie
    alert("The UserName is: " + getCookie("UserName"))

  }


  function removeUserName() {

    //delete the UserName from the cookie
    deleteCookie("UserName");
  }

//These functions work with the UserID

  function getUserID() {

    //get userID from prompt window
    var strID = window.prompt("Hello, What is your user ID?", "");

    //create cookie element named 'UserID' with a value stored in strID
    addCookie("UserID",strID);

  }


  function displayUserID () {

    //display the UserID element of the cookie
    alert("The UserID is: " + getCookie("UserID"))

  }


  function removeUserID() {

    //delete the UserID from the cookie
    deleteCookie("UserID");
  }



</script>

</head>

<body>
 <h1>Cookie Home</h1>
  <a href="cookieHandHome.php">My Home Page</a>

 <p><input type="button" name="btnDisplayCookie" value="Display Cookie"  onClick="displayCookie()"> </p>

<HR> 

 <p><input type="button" name="btnUserName" value="Add UserName to Cookie"  onClick="getUserName()"> </p>

 <p><input type="button" name="btnDisplayUserName" value="Display  UserName from Cookie"  onClick="displayUserName()"> </p>

 <p><input type="button" name="btnDeleteUserName" value="Delete UserName from Cookie"  onClick="removeUserName()"> </p>

<HR>

 <p><input type="button" name="btnUserID" value="Add UserID to Cookie"  onClick="getUserID()"> </p>

 <p><input type="button" name="btnDisplayUserID" value="Display  UserID from Cookie"  onClick="displayUserID()"> </p>

 <p><input type="button" name="btnDeleteUserID" value="Delete UserID from Cookie"  onClick="removeUserID()"> </p>

</body>
</html>