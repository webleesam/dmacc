<!DOCTYPE html>
<html ng-app="recipe">

<head>
    <meta charset="utf-8" />
    <title>AngularJS Recipe</title>
    <link rel="stylesheet" type="text/css" href="recipe.css">

    <!-- <script>document.write('<base href="' + document.location + '" />');</script> -->

    <!-- <script data-require="angular.js@1.0.x" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular.min.js" data-semver="1.0.8"></script> -->

	<!-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script> -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular.min.js"></script>

	<script src="app.js"></script>
    <script src="recipe.js"></script>

</head>

<body>

    <p>Select page content template via dropdown</p>
    <select ng-model="template">
        <option value="page1">Page 1 - Boilermaker Chili</option>
        <option value="page2">Page 2 - Diann Chilli</option>
        <option value="page3">Page 3 - Italian Vegetable</option>
        <option value="page4">Page 4 - Slow Cooker Taco</option>
    </select>

    <p>Set page content template via button click</p>
    <button ng-click="template='page1'">Show Page 1 Content</button>
    <button ng-click="template='page2'">Show Page 2 Content</button>
    <button ng-click="template='page3'">Show Page 3 Content</button>
    <button ng-click="template='page4'">Show Page 4 Content</button>

    <ng-include src="template"></ng-include>

    <script type="text/ng-template" id="page1">
        <h1 style="color: blue;">This is the page 1 content</h1>
        <div ng-include="'soupBoilermakerChili.html'"></div>
    </script>

    <script type="text/ng-template" id="page2">
        <h1 style="color:green;">This is the page 2 content</h1>
        <div ng-include="'soupDiannChili.html'"></div>
    </script>

    <script type="text/ng-template" id="page3">
        <h1 style="color:red;">This is the page 3 content</h1>
        <div ng-include="'soupItalianVegetable.html'"></div>
    </script>

    <script type="text/ng-template" id="page4">
        <h1 style="color:red;">This is the page 4 content</h1>
        <div ng-include="'soupSlowCookerTaco.html'"></div>
    </script>

</body>

</html>