import {Component} from "angular2/core";

@Component({
   selector:'my-list',
   template:`<h2>List of Fruits</h2>
   <ol>
      <li *ngFor="#myItem of itemList">{{myItem.name}}</li>
   </ol>
   `
})

// @Component({
//    selector:'my-list2',
//    template:`<h2>List of Fruits</h2>
//    <ol>
//       <li *ngFor="#myItem of itemList">{{myItem.name}}</li>
//    </ol>
//    `
// })
export class ItemComponent{
   public itemList = [
      {name:"Apple"},
      {name:"Orange"},
      {name:"Grapes"},
      {name:"Water"},
   ];
}