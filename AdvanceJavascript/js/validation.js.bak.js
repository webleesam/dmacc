//This JS file checks validation
// Check to see if Javascript support file has loaded. Send alert
function checkFileLoad(){
	alert("Javascript support files loaded")
}


$(document).ready(function() {
	validForm = true;
	var xId = "Name";
	var yId = "Phone";
	resetErrorMessage(xId);
	resetErrorMessage(yId);
	//$("#error"+xId).html("");
	//$("#error"+yId).html("");
	$("#fileComplaintButton").click(function() {
		validForm = true;
		var x = $("#customer"+xId).val();
		var y = $("#customer"+yId).val();
		checkAll(x,y,xId,yId);
		//checkLength(x,xId);
		//checkLength(y,yId);
		checkLengthMore(x,xId);
		//checkLengthMore(y,yId);
		checkNumeric(y,yId);
		checkLengthPhone(y,yId)
		checkMaxLength(x,xId);
		alert("Forn valitation is "+validForm+" from the click of the button.");

	});
});

function resetErrorMessage(j) {
	if (validForm == true) {
		$("#error"+j).html("");	
	}
	
}

function checkAll(x,y,xId,yId) {
	//alert(x+y+xId+yId);
	x = x.trim().replace(/\s+/g, ' '); /*It replaces one OR more with one space*/
	y = y.trim().replace(/\s+/g, ' ');
	//xId = xId;
	//yId = yId;
	if (x.length < 1 || y.length < 1) {
		validForm = false;
		//alert("test"+" "+x+" "+y);
	} else {
		//alert("hello"+" "+x+" "+y);
		
	}
	//alert(validForm+" FROM checkAll"+" "+x+" "+y)
}

function checkLength(i,j) {


	if (i.length < 1) {
		validForm = false;
		$("#error"+j).html("Data cannot be blank");
	} else {
		
	}
	//alert(validForm+" FROM checkLength"+" "+i)
}

function checkLengthMore(i,j) {
	if (i.length < 2) {
		validForm = false;
		$("#error"+j).html("Data must be more than two digits");	
		} else {
		resetErrorMessage(j);	
		}
		//alert(validForm+" FROM checkLengthMore"+" "+i+j)
}

function checkNumeric(i,j) {
	if (!$.isNumeric(i)) {
		validForm = false;
		//alert("is NOT Numeric");
		
	} else {
		//alert("is Numeric");
	}
}

function checkLengthPhone(i,j) {
	if (i.length != 10) {
		validForm = false;
		$("#error"+j).html("Data must be more than 10 digits only. No letters, or special charaters ( ) . - ");	
		} else {
		resetErrorMessage(j);	
		}
		//alert(validForm+" FROM checkLengthMore"+" "+i+j)
}

function checkMaxLength(i,j) {
	if (i.length > 30) {
		validForm = false;
		$("#error"+j).html("Data must NOT be more than 30 digits long.");
	} else {
		resetErrorMessage(j);
	}
}

/*

function checkData(){
	var myVal = $("#inVal").val().trim().replace(/\s+/g, ' '); /*It replaces one OR more with one space
	alert("test");								/*http://stackoverflow.com/questions/6163169/replace-multiple-whitespaces-with-single-whitespace-in-javascript-string
	if (myVal == "") {
		alert("Value cannot be blank");
		$("#error").html("Data cannot be blank");
	} else {
		if (myVal.length < 2) {
			$("#error").html("Data must be greater than one letter");	
		} else {
			var nlength = myVal.length;
			var whitespace = myVal.indexOf(" ");
			alert(nlength);
			alert(whitespace);
			if (whitespace > 0) {
				$("#error").html("Data must be only one word");
			} else {
				alert("Data Good");
				alert(myVal);
				$("#theData").html(myVal);	
				$("#error").html("Double click to clear input data");	
			}	
		}
	}
}	
*/

/*




$(document).ready(function() {
	$("#inVal").dblclick(function() {
		$("#inVal").val("");
	});
	
	// 1. Number must be numeric, no symbols (no periods)
	// 2. Not negitive
	// 3. 
	
	$("#checkPhone").click(function () {
		var phoneVal = $("#xPhone").val().trim().replace(/\s+/g, ' ');
		alert(phoneVal);
		if ($.isNumeric( phoneVal )) {
			alert("true");
		} else {
			alert("fslse");
		}
	});	
	
	
});*/