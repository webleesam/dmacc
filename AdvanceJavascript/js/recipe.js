$(document).ready(function(){
    /*watch for the choice to be checked*/
    $(":checkbox").click(function(){
        //alert("hello");
        //alert("The chicken radio button was clicked.");
        //var checkVal = $('#chicken').is(":checked");

            // This is making an associative array
        var checkVal = {soupName:$(this).val(),soupCheck:$(this).is(":checked")};
        
        //alert(checkVal["soupName"]+" "+checkVal["soupCheck"]);
        //alert(this.checked);
        //$("#soupSlowCookerTaco").css("display","inherit");
        showRecipe(checkVal);
    });
        $(":radio").click(function(){
        var checkSizeVal = {soupSizeName:$(this).val(),soupSizeCheck:$(this).is(":checked")};   
        showSizeRecipe(checkSizeVal);
    });

    $("button#ingredientListBoilermakerChili").click(function ingredientListBoilermakerChilitoggle() {
        $('#ingredientBoilermakerChili').toggle();
        $('#ingredientBoilermakerChiliHead').toggle();
    })

    $("button#directionsListBoilermakerChili").click(function ingredientListBoilermakerChilitoggle() {
        $('#directionsBoilermakerChili').toggle();
    })


    $("button#ingredientListDiannChili").click(function ingredientListDiannChilitoggle() {
        $('#ingredientDiannChili').toggle();
        $('#ingredientDiannChiliHead').toggle();
    })

    $("button#directionsListDiannChili").click(function ingredientListDiannChilitoggle() {
        $('#directionsDiannChili').toggle();
    })

    $("button#ingredientListSlowCookerTaco").click(function ingredientListSlowCookerTacotoggle() {
        $('#ingredientSlowCookerTaco').toggle();
        $('#ingredientSlowCookerTacoHead').toggle();
    })

    $("button#directionsListSlowCookerTaco").click(function ingredientListSlowCookerTacotoggle() {
        $('#directionsSlowCookerTaco').toggle();
    })


        $("button#ingredientListItalianVegetable").click(function ingredientListItalianVegetabletoggle() {
        $('#ingredientItalianVegetable').toggle();
        $('#ingredientItalianVegetableHead').toggle();
    })

    $("button#directionsListItalianVegetable").click(function ingredientListItalianVegetabletoggle() {
        $('#directionsItalianVegetable').toggle();
    })



});

/*show or hide the choice selected*/
function showRecipe(checkVal) {
    if (checkVal["soupName"]=="checkSlowCookerTaco") {
        if (checkVal["soupCheck"]==true) {
            $("#soupSlowCookerTaco").css("display","inherit");
        } else {
            $("#soupSlowCookerTaco").css("display","none"); 
        }  
    }
    if (checkVal["soupName"]=="checkItalianVegetable") {
        if (checkVal["soupCheck"]==true) {
            $("#soupItalianVegetable").css("display","inherit");
        } else {
            $("#soupItalianVegetable").css("display","none"); 
        }  
    }
    if (checkVal["soupName"]=="checkBoilermakerChili") {
        if (checkVal["soupCheck"]==true) {
            $("#soupBoilermakerChili").css("display","inherit");
        } else {
            $("#soupBoilermakerChili").css("display","none"); 
        }  
    }
    if (checkVal["soupName"]=="checkDiannChili") {
        if (checkVal["soupCheck"]==true) {
            $("#soupDiannChili").css("display","inherit");
        } else {
            $("#soupDiannChili").css("display","none"); 
        }  
    }
}

/*show or hide the choice selected*/
function showSizeRecipe(checkSiveVal) {
    if (checkSiveVal["soupSizeName"]=="checkSlowCookerTaco") {
        if (checkSiveVal["soupSizeCheck"]==true) {
            $("#soupSlowCookerTaco").css("display","inherit");
        } else {
            $("#soupSlowCookerTaco").css("display","none"); 
        }  
    }
    if (checkSiveVal["soupSizeName"]=="checkItalianVegetable") {
        if (checkSiveVal["soupSizeCheck"]==true) {
            $("#soupItalianVegetable").css("display","inherit");
        } else {
            $("#soupItalianVegetable").css("display","none"); 
        }  
    }
    if (checkSiveVal["soupSizeName"]=="checkBoilermakerChili") {
        if (checkSiveVal["soupSizeCheck"]==true) {
            $("#soupBoilermakerChili").css("display","inherit");
        } else {
            $("#soupBoilermakerChili").css("display","none"); 
        }  
    }
}


//soupSlowCookerTacoDouble