$(document).ready(function(){
	$("#myButtonNumber").click(function(){
		checkNumber = $("#myNumber").val();
		testNumber();
    });
});

function testNumber(){
	var myRegEx = /^\d{1,}$/; // Used to test to see if number
	if (checkNumber.length > 0) {
		if (myRegEx.test(checkNumber)) {
			$("#myNumberResult").html("That is a valid number. You entered <strong>"+checkNumber+"</strong>").css('color', 'blue');
		} else {
			$("#myNumberResult").html("That is <strong>not</strong> a valid number. You entered <strong>"+checkNumber+"</strong>").css('color', 'red');

		}	
	} else {
		$("#myNumberResult").html("You did not enter anything").css('color', 'red');	
	}
	

}


$(document).ready(function(){
	$("#myButtonEmail").click(function(){
		checkEmail = $("#myEmail").val();
		testRegExEmail();
        //alert("test");
    });
});	

function testRegExEmail(){
	var myRegEx = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i; // Used to test to see if email
	if (checkEmail.length > 0) {
		if (myRegEx.test(checkEmail)) {
			$("#myEmailResult").html("That is a valid Email. You entered <strong>"+checkEmail+"</strong>").css('color', 'blue');
		} else {
			$("#myEmailResult").html("That is <strong>not</strong> a valid Email. You entered <strong>"+checkEmail+"</strong>").css('color', 'red');

		}	
	} else {
		$("#myEmailResult").html("You did not enter anything").css('color', 'red');	
	}

}


$(document).ready(function(){
	$("#myButtonPhone").click(function(){
		checkPhone = $("#myPhone").val();
		testRegExPhone();
        //alert("test");
    });
});	

function testRegExPhone(){
	var myRegEx = /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/; // Used to test to see if Phone
	if (checkPhone.length > 0) {
		if (myRegEx.test(checkPhone)) {
			$("#myPhoneResult").html("That is a valid Phone. You entered <strong>"+checkPhone+"</strong>").css('color', 'blue');
		} else {
			$("#myPhoneResult").html("That is <strong>not</strong> a valid Phone. You entered <strong>"+checkPhone+"</strong>").css('color', 'red');

		}	
	} else {
		$("#myPhoneResult").html("You did not enter anything").css('color', 'red');	
	}

}

$(document).ready(function(){
	$("#myButtonZipcode").click(function(){
		checkZipcode = $("#myZipcode").val();
		testRegExZipcode();
        //alert("test");
    });
});	

function testRegExZipcode(){
	var myRegEx = /^\d{5}(-\d{4})?$/; // Used to test to see if Zipcode
	if (checkZipcode.length > 0) {
		if (myRegEx.test(checkZipcode)) {
			$("#myZipcodeResult").html("That is a valid Zipcode. You entered <strong>"+checkZipcode+"</strong>").css('color', 'blue');
		} else {
			$("#myZipcodeResult").html("That is <strong>not</strong> a valid Zipcode. You entered <strong>"+checkZipcode+"</strong>").css('color', 'red');

		}	
	} else {
		$("#myZipcodeResult").html("You did not enter anything").css('color', 'red');	
	}

}

$(document).ready(function(){
	$("#myButtonSpecialCharacter").click(function(){
		checkSpecialCharacter = $("#mySpecialCharacter").val();
		//alert(checkSpecialCharacter);
		//alert(checkSpecialCharacter.length);
		testSpecialCharacter();
        //alert("test");
    });
});	

function testSpecialCharacter(){
	var myRegEx = />|<|\\|\//g; // Used to test to see if forward slash
	if (checkSpecialCharacter.length > 0) {
		//alert(myRegEx.test(checkSpecialCharacter));
		if (myRegEx.test(checkSpecialCharacter)) {
			//alert(checkSpecialCharacter);
			//alert(checkSpecialCharacter.length);

			//alert("hello");
			var replacementCharacter = checkSpecialCharacter.replace(myRegEx, '-');
			//alert(replacementCharacter);
			$("#mySpecialCharacterResult").html("That is the correct special character. You entered <strong>"+checkSpecialCharacter+"</strong> and we replaced it with <strong>"+replacementCharacter+"</strong>").css('color', 'blue');
		} else {
			//alert("bye");
			$("#mySpecialCharacterResult").html("That is <strong>not</strong> the correct special character. You entered <strong>"+checkSpecialCharacter+"</strong>").css('color', 'red');
		}	
	} else {
		$("#mySpecialCharacterResult").html("You did not enter anything").css('color', 'red');	
	}

}