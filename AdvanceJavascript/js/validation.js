//This JS file checks validation
// Check to see if Javascript support file has loaded. Send alert
function checkFileLoad() {
	//alert("Javascript support files loaded")
}


$(document).ready(function() {
	// Set Form to True to begin processing
	validForm = true;
	// Set varible names to pas into functions. This keeps from having to write a function for each field
	var nameId = "Name";
	var phoneId = "Phone";
	var complaintId = "Complaint";
	// Reset all error messages when page loads
	resetErrorMessage(nameId);
	resetErrorMessage(phoneId);
	resetErrorMessage(complaintId);
	// Watch for button to be clicked to run form processing
	$("#fileComplaintButton").click(function() {
		validForm = true;
		// Get values for all fields and assign to varible to be passed into functions
		var nameVal = $("#customer"+nameId).val();
		var phoneVal = $("#customer"+phoneId).val();
		var complaintVal = $("#customer"+complaintId).val();
		// Run processing functions based on required validation
		checkAll(nameId,nameVal,phoneId,phoneVal,complaintId,complaintVal);
		checkMinLength(nameId,nameVal);
		checkMinLength(phoneId,phoneVal);
		checkMinLength(complaintId,complaintVal);
		checkMaxLength(nameId,nameVal);
		checkMaxLength(phoneId,phoneVal);
		checkMaxLength(complaintId,complaintVal);
		checkNum(nameId,nameVal);
		checkNum(phoneId,phoneVal);
		checkNum(complaintId,complaintVal);

		//alert("Form valitation is "+validForm+" from the click of the button.");
		// Display a pop up div on valid form
		if (validForm == true) {
			$("#formPopUp").html("<h1>Form Validation is Correct</h1><p>click to close</p>");
			$("#formPopUp").css("display","block");
		}
	});
	// Close pop up form when clicked
	$("#formPopUp").click(function() {
		$("#formPopUp").css("display","none");
	});

});

function resetErrorMessage(id,idVal,idValState) {
	// Reset all errors on valid form
	if (validForm == true) {
		$("#error"+id).html("");
	}
	// Reset form field on valid data
	if (idValState == true) {
		$("#error"+id).html("");
		
	}
	
}

function checkAll(nameId,nameVal,phoneId,phoneVal,complaintId,complaintVal) {
	// Check all fields for data
	nameVal = nameVal.trim().replace(/\s+/g, ' '); /*It replaces one OR more with one space*/
	phoneVal = phoneVal.trim().replace(/\s+/g, ' ');
	complaintVal = complaintVal.trim().replace(/\s+/g, ' ');
	if (nameVal.length < 1 || phoneVal.length < 1 || complaintVal.length < 1) {
		validForm = false;
		
	} 
}

// Check each field for minimum lenght of data. Use varibles for easy change
function checkMinLength(id,val) {
	idVal = id+"Val";
	idValState = true;
	switch (id) {
		case "Name":
			minLen = 2;
			break;
		case "Phone":
			minLen = 10;
			break;
		case "Complaint":
			minLen = 5;
			break;	
	}
	
	if (val.length < minLen) {
		validForm = false;
		
		idValState = false;
		$("#error"+id).html("Data cannot be less than "+minLen+" charaters long!! You only entered "+val.length+" characters.");
	} else {
		resetErrorMessage(id,idVal,idValState);
	}
}
// Check each field for maximum lenght of data. Use varibles for easy change
function checkMaxLength(id,val) {
	switch (id) {
		case "Name":
			maxLen = 20;
			break;
		case "Phone":
			maxLen = 10;
			break;
		case "Complaint":
			maxLen = 200;
			break;	
	}
	
	if (val.length > maxLen) {
		validForm = false;
		$("#error"+id).html("Data cannot be more than "+maxLen+" charaters long!! You only entered "+val.length+" characters.");
	} else {
		resetErrorMessage(id);
	}
	
}
// Check each field for to see if number. Use varibles for easy change
function checkNum(id,val) {
	switch (id) {
		case "Name":
		if ($.isNumeric(val)) {
			validForm = false;
			$("#error"+id).html("Data cannot be Number!! You  entered "+val+".");
		}
		break;

		case "Phone":
		if (!$.isNumeric(val)) {
			validForm = false;
			$("#error"+id).html("Data must be Number!! No - . ( ) You  entered "+val+".");
		}
		break;

		case "Complaint":
		if ($.isNumeric(val)) {
			validForm = false;
			$("#error"+id).html("Data cannot be Number!! You  entered "+val+".");
		}
		break;

	}

}