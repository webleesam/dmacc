<?php require "OppMail.class.php" ?>
<?php
	$newEmail = new Email();

	$newEmail->setFromAddress('sales@webleesam.com');
	$newEmail->setToAddress('info@webleesam.com');
	$newEmail->setSubject('EMAIL Timesheet');
	$newEmail->setMessage('Here is the body of the message. It is using Object Oriented Programming to send the email using an OOP Class file. ');

	$myFrom = $newEmail->getFromAddress();
	$myTo = $newEmail->getToAddress();
	$mySubject = $newEmail->getSubject();
	$myMessage = $newEmail->getMessage();

	$mySentStatus = $newEmail->SendEmail($myTo,$mySubject,$myMessage,$myFrom);

 ?>
 <!DOCTYPE html>
<html>
<head>
<title>Email Timesheet</title>
<link rel="stylesheet" type="text/css" href="css/assignmentStyle.css">
</head>

<body>
  <h1>Email Timesheet</h1>

  <table>

  	<tr>
  		<td>The email was sent to <?php echo $myTo;  ?></td>
  	</tr>

  	<tr>
  		<td>The subject of the email was <?php echo $mySubject;  ?></td>
  	</tr>

  	<tr>
  		<td>The message of the email was <?php echo $myMessage;  ?></td>
  	</tr>

  	<tr>
  		<td>The sent Status of the email was <?php echo $mySentStatus;  ?></td>
  	</tr>

  </table>  

  <footer>
    <a href='logOn.php'>Log On</a><br>
    <a href='logOut.php'>Log Out</a><br>
    <a href='#' onClick='history.go(-1);return true;'>Go Back</a><br>
    <a href='../wdv341.php'>Main Homework Page</a><br>
    <a href='../'>Return to Root Folder</a><br>
  </footer>
</body>
</html>