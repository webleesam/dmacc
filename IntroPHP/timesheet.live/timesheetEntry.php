<?php
session_start();
if ($_SESSION['validUser'] == "yes") {
	$name = $_SESSION['userName'];
	$logOnMessage = "Welcome Back! $name<br>";

	// Self Posting Page
	// 1st add (define) variables
	
	$date = "Click here to select date";
	$dateError="";
	$name = "";
	$nameError="";
	$jobnumber = "Enter in a Job Number";
	$jobnumberError="";
	$jobname = "Enter in a Job Name";
	$jobnameError="";
	$rt = 0;
	$rtError="";
	$ot = 0;
	$otError="";
	$dt = 0;
	$dtError="";

	$mySentStatus="TEST TEXT HERE TO BE REPLACED";

	// 2nd set form valid to false. assume form is false, let validation functions 	change it to true
	
	$validForm=false;
	//if ($validForm) {echo "form valid:  <br>";}
	

	// 3rd create validation functions. must pass (define) variables globally
	
	function validateBlank($x) {
  		global $validForm;
  		$xError = ""; 
  		if(empty($x)) {
  			$validForm = false;         
    		$xError .= "Input Field cannot be blank!"; 
    		return $xError; 
  		} 
	}
	
	function validateSpecial($x) {
  		global $validForm;
  		$xError = "";
		if (preg_match("([\'\/~`\!@#\$\"\\%\^&\*\(\)_\-\+=\{\}\[\]\|;:<\>,\.\?])",$x)) {  //From http://stackoverflow.com/questions/3937569/preg-match-php-special-characters
  			$validForm = false;
  			$xError .= "Input Field contains an illegal character!"; 
    		return $xError;
 		}
	}
	
	function validateNonNumber($x) {
  		global $validForm;
  		$xError = "";
  		if (!empty($x)) { 
    		if (!preg_match("/^[a-zA-Z\s]+$/",$x)) {  echo "not match<br>";
     			$validForm = false;
      			$xError .= "Input Field contains Letters!"; 
      			return $xError;
   			 } 
 		 }  
	}
	
	function validateYesNumber($x) {
  		global $validForm;
  		$xError = "";
  		if (!empty($x)) { 
    		if (preg_match("/^[a-zA-Z\s]+$/",$x)) {
      			$validForm = false;
      			$xError .= "Input Field contains Number!"; 
      			return $xError;
    		} 
 	 	}  
	}
	
	function validateName ($x) {
		global $nameError;
		$nameError .= validateBlank($x);
		$nameError .= validateSpecial($x);
		$nameError .= validateNonNumber($x);
	}
	
	function validateDate ($x) {
		global $dateError;
		$dateError .= validateBlank($x);
	}
	
	function validateJobNumber ($x) {
		global $jobnumberError;
		$jobnumberError .= validateBlank($x);
		$jobnumberError .= validateSpecial($x);
		$jobnumberError .= validateYesNumber($x);
	}
	
	function validateJobName ($x) {
		global $jobnameError;
		$jobnameError .= validateBlank($x);
		$jobnameError .= validateSpecial($x);
		$jobnameError .= validateNonNumber($x);
	}
	
	function validateRt ($x) {
		global $rtError;
		$rtError .= validateSpecial($x);
		$rtError .= validateYesNumber($x);
	}
	
	function validateOt ($x) {
		global $otError;
		$otError .= validateSpecial($x);
		$otError .= validateYesNumber($x);
	}
	
	function validateDt ($x) {
		global $dtError;
		$dtError .= validateSpecial($x);
		$dtError .= validateYesNumber($x);
	}



	
	
	if (isset($_POST["submit"])){
		$date = ($_POST["date"]);
		if ($date == "Click here to select date") {
			$date = "";
		}
		$name = ($_POST["name"]);
		$jobnumber = ($_POST["jobnumber"]);
		if ($jobnumber == "Enter in a Job Number") {
			$jobnumber = "";
		}
		$jobname = ($_POST["jobname"]);
		if ($jobname == "Enter in a Job Name") {
			$jobname = "";
		}
		$rt = ($_POST["rt"]);
		$ot = ($_POST["ot"]);
		$dt = ($_POST["dt"]);

		// 2nd set form valid to true. assume form is true, let validation functions change 	it to false
		$validForm=true;
		validateName ($name);
		validateDate ($date);
		validateJobNumber ($jobnumber);
		validateJobName ($jobname);
		validateRt ($rt);
		validateOt ($ot);
		validateDt ($dt);
	
	} 
	
	
	if ($validForm==true) {
		// connect to database when form is valid - check if local or live (on 	WebLeeSam.com)
		$ip = $_SERVER['SERVER_ADDR'];
	
		switch ($ip) {
			case "104.168.167.168" : 
			//require "dataBaseConnect.web.php";
			require "dataBaseConnect.web.timesheet.php";
			break;
	
			case "173.17.96.138" : 
			echo "not live<br>";
			//require "dataBaseConnect.local.php";
			require "dataBaseConnect.local.php";
			break;
	
			case "192.168.1.20" : 
			echo "Home Server<br>";
			//require "dataBaseConnect.local.php";
			require "dataBaseConnect.local.timesheet.php";
			//require "db.inc.php";
			//require "func.inc.php";
			//connect();
			break;
	
			case "::1" : 
			echo "localhost<br>";
			require "dataBaseConnect.local.php";
			break;
	
			default : 
			echo "There is no match";
			break;
		}
	
		$stmt = $con->prepare("INSERT INTO weekly (name, date, jobname, jobnumber, rt, ot, dt) VALUES (?, ?, ?, ?, ?, ?, ?)") or die("Program Killed 02");
	
		$stmt->bind_param("sssiiii", $name, $date, $jobname, $jobnumber, $rt, $ot, $dt) 	or die("Program Killed 02.1");
	
		$stmt->execute();
	
		$logOnMessage = "New records created successfully<br>";
	
		$stmt->close();
		$con->close();
		

	  	?>
	  	<form id="emailButton" action="emailTimesheet.php" method="post">
	  	<p>Once you have enter in your time press this <input type="submit" name="emailSubmit" value="Email Button" /> to notify your manager to review your timesheet</p>
	  	</form>
	  	<?php



	} else { // valid form

	
		?>	
		<!DOCTYPE html>
		<html>
		<head>
	    <title>Time Sheet Entry Form</title>
	    <link rel="stylesheet" type="text/css" href="style/style.css">
	    <style type="text/css">
	  		label, input, span { margin: 5px; }
	  		.error  { color:red; font-style:italic; }
	  		#suggest {padding-top: 7px;}
		</style>

		<!-- jQuery Datepicker Widget   -->
				<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/	smoothness/jquery-ui.css">
				<script src="//code.jquery.com/jquery-1.10.2.js"></script>
				<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
				<script>
				$(function() {
				$( "#datepicker" ).datepicker( {
					dateFormat: 'yy-mm-dd',
					} );
				});
				</script>
		<script src="js/script.js"></script>		
		</head>
		<body>
			<div id="container">
			<h1>Scope of Project</h1>
			<h2><?php echo $logOnMessage; ?></h2>
			<p>Employee timesheet. Name input field ulilizes jQuery and AJAX to call all 	available names from the database. Type in a letter to filter the list. Type 	in a new name to add a new name to the database.</p>
			
				<div id="myForm">
					<form id="entryForm" action="timesheetEntry.php" method="post">
						<div id="nameBox">
							<p><span class="error"><?php echo $nameError; ?></span>
							Name: <input id="name" name="name" onClick="Clear(); autoComplete()" onkeyup="autoComplete()" autocomplete="off" value="<?php echo $name; ?>"></input></p>
							<div id="suggest"></div>	
						</div>
						<div id="entry" class="row">
							<div>Date:<input type="text" id="datepicker" name="date" size="25" value="<?php echo $date; ?>"><span class="error"><?php echo $dateError; ?></span></div>
							<div>Job Number: <input type="text" name="jobnumber" onClick="ClearNumber();" id="jobnumber" size="25" value="<?php echo $jobnumber; ?>" /><span class="error"><?php echo $jobnumberError; ?></span></div>
							<div>Job Name: <input type="text" name="jobname" onClick=" ClearName();" id="jobname" size="25" value="<?php echo $jobname; ?>" /><span class="error"><?php echo $jobnameError; ?></span></div>
							<div>RT: <input type="text" name="rt" size="3" value="<?php echo $rt; ?>" /><span class="error"><?php echo $rtError; ?></span></div>
							<div>OT: <input type="text" name="ot" size="3" value="<?php echo $ot; ?>" /><span class="error"><?php echo $otError; ?></span></div>
							<div>DT: <input type="text" name="dt" size="3" value="<?php echo $dt; ?>" /><span class="error"><?php echo $dtError; ?></span></div>
						</div>
					<input type="submit" name="submit" value="Submit" />
					<input type="reset" name="reset" value="Reset" />
					</form>

					<form id="emailButton" action="emailTimesheet.php" method="post">
					<p>Once you have enter in your time press this <input type="submit" name="emailSubmit" value="Email Button" /> to notify your manager to review your timesheet</p>
					</form>



				</div> <!-- end of myForm -->
				<hr>       <!--    Retrieve            -->
		<?php
	}	
} else {
	header('Location: logOn.php');
}

?>	
	 <footer>
    	<a href='timesheetEntry.php'>Visit Page Again</a><br>
    	<a href='logOn.php'>Log On</a><br>
    	<a href='logOut.php'>Log Out</a><br>
    	<a href='#' onClick='history.go(-1);return true;'>Go Back</a><br>
    	<a href='../wdv341.php'>Main Homework Page</a><br>
    	<a href='../'>Return to Root Folder</a><br>
  	</footer>
  	</div> <!-- end of container -->	
	</body>
	</html> 