<?php
$name="";
$ssn="";
$radioGroup="";

$nameError="";
$ssnError="";
$radioGroupError="";

$validForm=false;

function validateName()
{
  global $name, $nameError, $validForm;    // by using the keyword global these variables referance the varibles outside this function. otherwise these would have been created local variables only for this function
  // if($name=="")
  if(empty($name))  
  {
    //echo "Name is Empty<br>";
    $validForm = false;         
    $nameError = "Name is in Error, Name cannot be blank"; 
  } 
}



function validateSsn()
{
  global $ssn, $ssnError, $validForm;
  if (!preg_match("#\b[0-9]{3}-[0-9]{2}-[0-9]{4}\b#",$ssn)) {  //From http://stackoverflow.com/questions/4591171/regular-expression-to-match-social-security-number
    $validForm = false;
    $ssnError = "SSN is not in the correct format. It will need to be formated as XXX-XX-XXXX"; 
  }   
}



if (isset($_POST["submit"])){
  

  echo "Form has been submitted<br>";
  $name = ($_POST["name"]);
  //echo "echo ".$name."<br>";

  $ssn = ($_POST["ssn"]);
  //echo "echo ".$ssn."<br>";

  $validForm=true;
  validateName();
  validateSsn();

} // end of else statement for is set
else {
  
  echo "Form has NOT been submitted<br>";
  
} // End of Else for isset


if ($validForm==true) {
  echo "Form is Valid<br>";

  $name = ($_POST["name"]);
  //echo "echo ".$name."<br>";

  $ssn = ($_POST["ssn"]);
  //echo "echo ".$ssn."<br>";

  echo "<a href='formValidationAssignment.php'>Vist page again</a><br>";


} else {
  echo "Form is NOT Valid<br>";

?>

<!DOCTYPE html>
<html >
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>WDV341 Intro PHP - Form Validation Example</title>
  <link rel="stylesheet" type="text/css" href="css/assignmentStyle.css">
  <style>

    #orderArea  {
      width:900px;
      background-color:#CF9;
    }

    .error  {
      color:red;
      font-style:italic;  
    }
    table {
      width: auto;
    }

  </style>
</head>

<body>
  <a href="formValidationAssignment.php">Vist page again</a> 
  <h1>WDV341 Intro PHP</h1>
  <h2>Form Validation Assignment
  </h2>



  <div id="orderArea">

    <form id="form1" name="form1" method="post" action="formValidationAssignment.php">
    <!-- <form id="form1" name="form1" method="post" action="?"> -->
      <h3>Customer Registration Form</h3>
      <table width="787" border="0">
        <tr>
          <td width="117">Name:</td>
          <td width="246"><input type="text" name="name" id="name" size="40" value="<?php echo $name; ?>"/></td>
          <td width="410" class="error"><?php echo "$nameError"; ?></td>
        </tr>
        <tr>
          <td>Social Security</td>
          <td><input type="text" name="ssn" id="ssn" size="40" value="<?php echo $ssn; ?>" /></td>
          <td class="error"><?php echo "$ssnError"; ?></td>
        </tr>
      </table>

      <p>
        <input type="submit" name="submit" id="submit" value="Submit" />
        <input type="reset" name="reset" id="reset" value="Clear Form" />
      </p>

    </form>
  </div> <!-- end of order area -->

<?php include "include/footer.php" ?>
</body>
</html>
<?php
} // End of Else for Valid Form
?>