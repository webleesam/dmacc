<?php
	$tableData = "";	
	
	foreach($_POST as $key => $value)		
	{
		$tableData .= "<tr>";				
		$tableData .= "<td>$key</td>";		
		$tableData .= "<td>$value</td>";	
		$tableData .= "</tr>";				
	} 
 
	
	$firstName = $_POST["firstName"];	
	$lastName = $_POST["lastName"];
	$school = $_POST["school"];
	$mailTo = $_POST["email"];
	$age = $_POST["age"];
	$moreInfo = $_POST["moreInfo"];
	$mailingList = $_POST["mailingList"];
	$selectChoice = $_POST["selectChoice"];
	
	$mailSubject = "Contact Form";

	$mailFrom = "From: sales@webleesam.com";

	$mailTxt = <<<EMAIL
Thank you for visiting my page $firstName $lastName

You wrote:
$firstName
$lastName
$school
$mailTo
$age
$moreInfo
$mailingList 
$selectChoice
$mailFrom

The message was sent to $mailTo
with a mailSubject of $mailSubject

EMAIL;



$verifyMail = mail($mailTo,$mailSubject,$mailTxt,$mailFrom);

?>

<!DOCTYPE html>
<html>
<head>
<title>Assignment: Form Handler Proccessing</title>
<link rel="stylesheet" type="text/css" href="css/assignmentStyle.css">
</head>

<body>
<h1>This assignment name is Form Handler Proccessing</h1>
	<table>
    <tr>
    	<th>Field Name</th>
        <th>Value of Field</th>
    </tr>
	<?php echo $tableData;  ?>
	</table>
</p>
<h4>Display the values </h4>
<p>First Name: <?php echo $firstName; ?></p>
<p>Last Name: <?php echo $lastName; ?></p>
<p>School: <?php echo $school; ?></p>
<p>Email: <?php echo $mailTo; ?></p>
<p>Age: <?php echo $age; ?></p>
<p>Request more info: <?php echo $moreInfo; ?></p>
<p>Add to mailing list: <?php echo $mailingList; ?></p>
<p>Drop down selection: <?php echo $selectChoice; ?></p>

<div id="Xcontainer">
		<?php
		echo "Did your mail go = ".$verifyMail."<br>";
		echo $mailTxt."<br>";

		?>

	</div><!-- end of Xcontainer -->


<!-- 
//This code pulls the field name and value attributes from the Post file
//The Post file was created by the form page when it gathered all the name value pairs from the form.
//It is building a string of data that will become the body of the email

//          CHANGE THE FOLLOWING INFORMATION TO SEND EMAIL FOR YOU //  

	$toEmail = "recipient email";		//CHANGE within the quotes. Place email address where you wish to send the form data. 
										//Use your DMACC email address for testing. 
										//Example: $toEmail = "jhgullion@dmacc.edu";		
	
	$subject = "WDV101 Email Example";	//CHANGE within the quotes. Place your own message.  For the assignment use "WDV101 Email Example" 

	$fromEmail = "senders email";		//CHANGE within the quotes.  Use your DMACC email address for testing OR
										//use your domain email address if you have Heartland-Webhosting as your provider.
										//Example:  $fromEmail = "contact@jhgullion.org";  

//   DO NOT CHANGE THE FOLLOWING LINES  //

	$emailBody = "Form Data\n\n ";			//stores the content of the email
	foreach($_POST as $key => $value)		//Reads through all the name-value pairs. 	$key: field name   $value: value from the form									
	{
		$emailBody.= $key."=".$value."\n";	//Adds the name value pairs to the body of the email, each one on their own line
	} 
	
	$headers = "From: $fromEmail" . "\r\n";				//Creates the From header with the appropriate address

 	if (mail($toEmail,$subject,$emailBody,$headers)) 	//puts pieces together and sends the email to your hosting account's smtp (email) server
	{
   		echo("<p>Message successfully sent!</p>");
  	} 
	else 
	{
   		echo("<p>Message delivery failed...</p>");
  	}

?> -->
<footer>
	<p>Click<a href="#" onClick="history.go(-1);return true;">Here</a> to go back one page</p>
	<p>Click <a href="./wdv341.php">Here</a> to return to the PHP Homework Page</p></footer>
</body>
</html>
