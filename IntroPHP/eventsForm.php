<?php

/*
This page will do two things:

Gather the information for an event using a form.

    It will display a form that can be used to input the informatin for an event.
    Use your database field names as your name attributes in the HTML of the form. 
    The form will use the post method and the action attribute will call eventsForm.php.  This is referred to as a self posting form.

Use PHP and SQL to process the form data into your database. 

    If the form has been submitted the page will use PHP to process the data from the form using the $_POST variable.
    It will connect to your database using your dbConnect.php page.  Include this into your page.
    It will create an SQL INSERT query using your form data.
    It will run that query to place the form data into your database on the wdv341_events table. 
*/

// Self Posting Page
// 1st add (define) variables

$myInput = "";
$myInputError="";
$studentName = "";
$studentNameError="";
$studentAddress = "";
$studentAddressError="";
$studentEmail = "";
$studentEmailError="";




// 2nd set form valid to false. assume form is false, let validation functions change it to true

$validForm=false;

// 3rd create validattion functions. must pass (define) variables globally

function validateBlank($x)
{
  global $validForm;
  $xError = ""; 
  if(empty($x))  
  {
    $validForm = false;         
    $xError .= "Input Field cannot be blank!"; 
    return $xError; 
  } 
}

function validateSpecial($x)
{
  global $validForm;
  $xError = "";
	if (preg_match("([\'\/~`\!@#\$\"\\%\^&\*\(\)_\-\+=\{\}\[\]\|;:<\>,\.\?])",$x)) {  //From http://stackoverflow.com/questions/3937569/preg-match-php-special-characters
  	$validForm = false;
  	$xError .= "Input Field contains an illegal character!"; 
    return $xError;
 	}
}

function validateNonNumber($x)
{
  global $validForm;
  $xError = "";
  if (!empty($x)) { 
    if (!preg_match("/^[a-zA-Z\s]+$/",$x)) {  echo "not match<br>";
      $validForm = false;
      $xError .= "Input Field contains Number!"; 
      return $xError;
    } 
  }  
}

function validateEmail($x)
{
  global $validForm;
  $xError = "";
  if ((!filter_var($x, FILTER_VALIDATE_EMAIL)) && (!empty($x))) {  //From http://www.w3schools.com/php/php_form_url_email.asp
    $validForm = false;
    echo var_dump($validForm) . "<br>";
    $xError .= "Email Format is not Valid!"; 
    return $xError; 
  }   
}

function validateStudentName ($x)
{
  global $studentNameError;
  $studentNameError .= validateBlank($x);
  $studentNameError .= validateSpecial($x);
	$studentNameError .= validateNonNumber($x);


}

function validateStudentAddress ($x)
{
  global $studentAddressError;
  $studentAddressError .= validateBlank($x);
  $studentAddressError .= validateSpecial($x);

}

function validateStudentEmail ($x)
{
  global $studentEmailError;
  $studentEmailError .= validateBlank($x);
  $studentEmailError .= validateEmail($x);

}

if (isset($_POST["submit"])){
  echo "Form has been submitted<br>";
  $studentName = ($_POST["studentName"]);
  $studentAddress = ($_POST["studentAddress"]);
  $studentEmail = ($_POST["studentEmail"]);

// 2nd set form valid to true. assume form is true, let validation functions change it to false
  $validForm=true;
  validateStudentName($studentName);
  validateStudentAddress($studentAddress);
  validateStudentEmail($studentEmail);

} // end of else statement for is set
else {
  
  echo "Form has NOT been submitted<br>";
  
} // End of Else for isset


if ($validForm==true) {
// connect to database when form is valid - check if local or live (on WebLeeSam.com)
  $ip = $_SERVER['SERVER_ADDR'];
  /*if ($ip == "104.168.167.168") {
    echo "live<br>";
    require "dataBaseConnect.web.php";
  } else {
    echo "local " . $ip ." - kill connection<br>";
    die("Program Killed 01");
  }*/

echo $ip . "<br>";
  switch ($ip)
  {
    case "104.168.167.168" : 
    echo "live<br>";
    require "dataBaseConnect.web.php";
    break;

    case "173.17.96.138" : 
    echo "not live<br>";
    require "dataBaseConnect.local.php";
    break;

    case "192.168.1.20" : 
    echo "Home Server<br>";
    require "dataBaseConnect.local.php";
    break;

    case "::1" : 
    echo "localhost<br>";
    require "dataBaseConnect.local.php";
    break;

    default : 
    echo "There is no match";
    break;
  }



// prepare and bind data and execute
	$stmt = $con->prepare("INSERT INTO wdv341_student (student_name, student_address, student_email) VALUES (?, ?, ?)") or die("Program Killed 02");
	$stmt->bind_param("sss", $studentName, $studentAddress, $studentEmail) or die("Program Killed 02.1");
  //$studentNameX ="Fred1";
  //$studentAddressX ="Fred2";
  //$studentEmailX ="Fred3";

	$stmt->execute(); //or die("Program Killed 03");
  //printf("Error: %s.\n", $stmt->error);


	echo "New records created successfully<br>";

	$stmt->close();
	$con->close();




// use for error checking	
  echo "Form is Valid<br>";

  //$myInput = ($_POST["myInput"]);
  // echo "echo ".$myInput." here<br>"; // use for error checking


} else {
// use for error checking
  echo "Form is NOT Valid<br>";


?>



<!DOCTYPE html>
<html>
<head>
	<title>eventsForm.php</title>
  <link rel="stylesheet" type="text/css" href="css/projectPageStyle.css">
  <link rel="stylesheet" type="text/css" href="css/eventsStyle.css">
  <style type="text/css">
    label, input, span { margin: 5px; }
  </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script>
  $(document).ready(function(){
      $("#clearForm").click(function(){
          $("#studentName").val("");
          $("#studentAddress").val("");
          $("#studentEmail").val("");
      });
  });
  </script>
</head>
<body>
<div id="container">
	<h1>Events Form</h1>
	<form id="myForm" name="myForm" method="post" action="eventsForm.php">
		<div>
			<label>Name: </label>
			<input type="text" id="studentName" name="studentName" value="<?php echo $studentName ?>"><span><?php echo $studentNameError ?>
		</div>
		<div>
			<label>Address: </label>
			<input type="text" id="studentAddress" name="studentAddress" value="<?php echo $studentAddress?>"><span><?php echo $studentAddressError ?>
		</div>
		<div>
			<label>Email: </label>
			<input type="text" id="studentEmail" name="studentEmail" value="<?php echo $studentEmail ?>"><span><?php echo $studentEmailError ?>
		</div>

		<input type="submit" name="submit" id="submit" value="Submit">
    <input type="reset" name="reset" id="reset" value="Reset">
    <input type="button" name="clearForm" id="clearForm" value="Clear Form">
	</form>

<?php
} // End of Else for Valid Form
?>


<!-- Used to go back one page or return to root folder -->
<footer>
  <a href='eventsForm.php'>Visit Page Again</a><br>
  <a href='#' onClick='history.go(-1);return true;'>Go Back</a><br>
  <a href='wdv341.php'>Main Homework Page</a><br>
  <a href='./'>Return to Root Folder</a><br>
</footer>

</div> <!-- end of container -->
</body>
</html>
