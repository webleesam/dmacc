<?php

/*The page will validate the form fields according to the following validation tests.

    Name Field:  Required, Check HTML special characters
    Email Field: Required, Must be a valid email format.  Use a Regular Expression to validate the format.
    Contact Reason:  An option must be selected. If Other is selected then Comments are Required and cannot be empty.
    Comments: Not required unless "Other" is selected then Required.  If anything is entered check for HTML special characters. 
    A form returned to the customer with errors will include all the original values and selections made by the customer along with any error messages.
    When the form data is valid it will continue with the following steps.

The page will create a confirmation page.  

    The confirmation page should display the information that was entered to the customer
    The content should be formatted in a customer friendly, easy to read format. 
    The page should display the time and date the contact was processed.

The page will create and send an email to the website owner's contact email.  

    Implement your Emailer class to perform this process.  Include the class into this page.
    Use your DMACC email addresses as the To: address.  You are acting as the client/business owner who needs to know their customer has a problem.
    Format the information so that it is easily read and looks professional.  This includes using line breaks and concatenation to create user friendly information.
    The email should display the time and date the contact was processed.*/




$name="";
$email="";
$selectChoice;
$comments="";

$selectChoiceMessage;

$nameError="";
$emailError="";
$selectChoiceError="";
$commentsError="";

$validForm=false;

function validateName()
{
  global $name, $nameError, $validForm;    // by using the keyword global these variables referance the varibles outside this function. otherwise these would have been created local variables only for this function
  // if($name=="")
  if(empty($name))  
  {
    //echo "Name is Empty<br>";
    $validForm = false;         
    $nameError = "Name is in Error, Name cannot be blank"; 
  } 
  // preg_match('/[\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\\]/', $input)
  // [\'\/~`\!@#\$%\^&\*\(\)_\-\+=\{\}\[\]\|;:"\<\>,\.\?\\]


  if (preg_match("([\'\/~`\!@#\$\"\\%\^&\*\(\)_\-\+=\{\}\[\]\|;:<\>,\.\?])",$name)) {  //From http://stackoverflow.com/questions/3937569/preg-match-php-special-characters
    $validForm = false;
    $nameError = "Name contains an illegal  is not in the correct format."; 
  }
}



function validateEmail()
{
  global $email, $emailError, $validForm;
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {  //From http://www.w3schools.com/php/php_form_url_email.asp
    $validForm = false;
    $emailError = "Email is not in the correct format."; 
  }   
}

function validateSelect()
{
  global $selectChoice, $selectChoiceError, $validForm, $commentsRequired, $selectChoiceMessage;
  if ($selectChoice == "default") {
    $validForm = false;
    $selectChoiceError = "Please make selection";
  }
  if ($selectChoice == "other") {
    $selectChoiceError = "";
    $selectChoiceMessage = "You have selected 'Other' as your reason for contact";
    validateComments();
  }
  if ($selectChoice == "product") {
    $selectChoiceMessage = "You have selected 'Product Problem' as your reason for contact";
  }
  if ($selectChoice == "return") {
    $selectChoiceMessage = "You have selected 'Return Product' as your reason for contact";
  }
  if ($selectChoice == "billing") {
    $selectChoiceMessage = "You have selected 'Billing Question' as your reason for contact";
  }
  if ($selectChoice == "technical") {
    $selectChoiceMessage = "You have selected 'Website Problem' as your reason for contact";
  }
}

function validateComments()
{
  global $comments, $commentsError, $validForm;    // by using the keyword global these variables referance the varibles outside this function. otherwise these would have been created local variables only for this function
  if(empty($comments))  
  {
    echo "Comments Empty<br>";
    $validForm = false;         
    $commentsError = "Comments in Error, Comments cannot be blank"; 
  } 


  if (preg_match("([\'\/~`\!@#\$\"\\%\^&\*\(\)_\-\+=\{\}\[\]\|;:<\>,\.\?])",$name)) {  //From http://stackoverflow.com/questions/3937569/preg-match-php-special-characters
    $validForm = false;
    $nameError = "Comments contains an illegal  is not in the correct format."; 
  }
}




if (isset($_POST["submit"])){
   $name = ($_POST["name"]);
   $email = ($_POST["email"]);
   $selectChoice = ($_POST["selectChoice"]);
   $comments = ($_POST["comments"]);


 // Used for Testing
  /*echo "Form has been submitted<br>";
  echo "echo ".$name."<br>";
  echo "echo ".$email."<br>";
  echo "echo ".$selectChoice."<br>";
  echo "echo ".$comments."<br>";*/


  $validForm=true;
  validateName();
  validateEmail();
  validateSelect();

} // end of else statement for is set

?>
<!DOCTYPE html>
<html >
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>WDV341 Intro PHP - Contact Form with Validation and Email</title>
  <link rel="stylesheet" type="text/css" href="css/assignmentStyle.css">
  <style>

    #orderArea  {
      width:900px;
      background-color:#CF9;
    }

    .error  {
      color:red;
      font-style:italic;  
    }
    table {
      width: auto;
    }

  </style>
</head>

<body>
  <a href="contactFormValidationProject.php">Vist page again</a> 
  <h1>WDV341 Intro PHP</h1>
  <h2>Contact Form Validation Project</h2>




<?php
if ($validForm==true) {

  // Used for testing
  /*echo "Form is Valid<br>";
  echo "echo ".$name."<br>";
  echo "echo ".$email."<br>";*/

  // This is where the confirmation page belongs
?>
  <h4>This is the HEADING of the Confirmation Page</h4>
  <p>Thank you for contacting us <b><?php echo $name; ?></b>. We have sent an email to <b><?php echo $email; ?></b> and we will follow-up on the issue related to:<br><b><?php echo $selectChoiceMessage; ?></b><br><b><?php echo $comments; ?></b><br>Your request was recieved on <?php echo date("D, M. d, Y") . " at " . date("h:i:s a"); ?></p>

<?php require "OppMail.class.php" ?>
<?php
$newEmail = new Email();

$newEmail->setFromAddress('sales@webleesam.com');
$newEmail->setToAddress($email);
$newEmail->setSubject('Contact Form Validation Project');
$newEmail->setMessage('Here is the body of the message sent from my OPP Mail function');

$myFrom = $newEmail->getFromAddress();
$myTo = $newEmail->getToAddress();
$mySubject = $newEmail->getSubject();
$myMessage = $newEmail->getMessage();

$mySentStatus = $newEmail->SendEmail($myTo,$mySubject,$myMessage,$myFrom);
echo "this is my send status ".$mySentStatus;

?>




<?php



} else {
  // Used for testing
  /*echo "Form is NOT Valid<br>";*/

?>





  <div id="orderArea">

    <form id="form1" name="form1" method="post" action="contactFormValidationProject.php">
    <!-- <form id="form1" name="form1" method="post" action="?"> -->
      <h3>Customer Registration Form</h3>
      <table width="787" border="0">
        <tr>
          <td width="117">Name:</td>
          <td width="246"><input type="text" name="name" id="name" size="40" value="<?php echo $name; ?>"/></td>
          <td width="410" class="error"><?php echo "$nameError"; ?></td>
        </tr>
        <tr>
          <td>Email:</td>
          <td><input type="text" name="email" id="email" size="40" value="<?php echo $email; ?>" /></td>
          <td class="error"><?php echo "$emailError"; ?></td>
        </tr>

        <tr>
          <td>Reason for contact:</td>
          <td>
            <select name="selectChoice" id="selectChoice">
              <option value="default" <?php if($selectChoice==""){echo "selected='selected'";}?>>Please Select a Reason</option>
              <option value="product" <?php if($selectChoice=="product"){echo "selected='selected'";}?>>Product Problem</option>
              <option value="return" <?php if($selectChoice=="return"){echo "selected='selected'";}?>>Return a Product</option>
              <option value="billing" <?php if($selectChoice=="billing"){echo "selected='selected'";}?>>Billing Question</option>
              <option value="technical" <?php if($selectChoice=="technical"){echo "selected='selected'";}?>>Report a Website Problem</option>
              <option value="other" <?php if($selectChoice=="other"){echo "selected='selected'";}?>>Other</option>
            </select>
          </td>
          <td class="error"><?php echo "$selectChoiceError"; ?></td>
        </tr>

        <tr>
          <td>Comments:</td>
          <td><textarea name="comments" id="comments" cols="45" rows="5"></textarea></td>
          <td class="error"><?php echo "$commentsError"; ?></td>
        </tr>



      </table>

      <p>
        <input type="submit" name="submit" id="submit" value="Submit" />
        <input type="reset" name="reset" id="reset" value="Clear Form" />
      </p>

    </form>
  </div> <!-- end of order area -->

<?php include "include/footer.php" ?>
</body>
</html>
<?php
} // End of Else for Valid Form
?>