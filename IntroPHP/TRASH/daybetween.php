<?php
$datetime1 = new DateTime('2009-10-11');
$datetime2 = new DateTime('2009-10-13');
$interval = $datetime1->diff($datetime2);
echo $interval->format('%R%a days');
?>

<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title>WDV341 Intro PHP - Days Between Two Dates</title>
	</head>

	<body>
		<h1>WDV341 Intro PHP</h1>
		<form name="form1" method="post" action="processDayBetween.php">
		    <label>First Date: <textarea name="firstDate" id="firstDate" cols="45" rows="5"></textarea></label>
        <label>Second Date: <textarea name="secondDate" id="secondDate" cols="45" rows="5"></textarea></label>
        <input type="submit" value="Submit">
	</form>
	</body>
</html>
