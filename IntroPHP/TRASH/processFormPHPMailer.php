<?php
require 'PHPMailer/PHPMailerAutoload.php';
// Data received from POST request
// $name = stripcslashes($_POST['name']);
$name = stripcslashes($_POST['yourName']);
// $emailAddr = stripcslashes($_POST['email']);
$emailAddr = stripcslashes($_POST["yourEmail"]);
// $issue = stripcslashes($_POST['issue']);
//$issue = "the issue";
// $comment = stripcslashes($_POST['message']);
$comments = stripcslashes($_POST['yourComments']);
// $subject = stripcslashes($_POST['subject']);   
$subject = "Contact Form";   

// Send mail
$mail = new PHPMailer();
$mail->IsSMTP(); // telling the class to use SMTP

// SMTP Configuration
$mail->SMTPAuth = true;                  // enable SMTP authentication
$mail->Host = "mail.webleesam.com"; // SMTP server
$mail->Username = "test@webleesam.com";
$mail->Password = "test";            
//$mail->Port = 465; // optional if you don't want to use the default 

$mail->From = "test@webleesam.com";
$mail->FromName = "Test";
$mail->Subject = $subject;
$mail->AltBody = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test



$txt = "<html><body>";
$txt .= "<h2>";
$txt .= "Thank you for visiting my page $name.<br>";
$txt .= "<br>";
$txt .= "You wrote $comments.<br>";
$txt .= "<br>";
$txt .= "The message was sent to $emailAddr.<br>";
$txt .= "with a subject of $subject.<br>";

$txt .= "</h2>";
$txt .= "</body></html>";

// $mail->MsgHTML($issue . "<br /><br />" . $comment);
$mail->MsgHTML($txt);

// Add as many as you want
$mail->AddAddress($emailAddr, $name);

// If you want to attach a file, relative path to it
//$mail->AddAttachment("images/phpmailer.gif");             // attachment

$response= NULL;
if(!$mail->Send()) {
	$response = "Mailer Error: " . $mail->ErrorInfo;
} else {
	$response = "Message sent!";
}

$output = json_encode(array("response" => $response)); 
?>

<!DOCTYPE html>
<html>
<head>
	<title>PHP Process Form</title>
	<link rel="stylesheet" type="text/css" href="css/projectPageStyle.css">
	<style>

	</style>
</head>

<body>
	<div id="container">
		<h1>Header for the proccess form page</h1>
		<?php echo("<h2>".$response."</h2>"); ?>


	</div><!-- end of container -->
	<footer>
	<p>Click<a href="#" onClick="history.go(-1);return true;">Here</a> to go back one page</p>
	<p>Click <a href="./wdv341.php">Here</a> to return to the PHP Homework Page</p></footer>
</body>
</html> 