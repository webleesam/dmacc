<?php
	$yourName = "Brian Coffie";
	$number1 = 5;
	$number2 = 8;
	$total = $number1 + $number2;
	$myArray = "<script>var cars = ['PHP', 'HTML', 'Javascript'];</script>"
	
	
	
?>
<!DOCTYPE html>
<html>
<head>
<title>Assignment: PHP Basics</title>
<link rel="stylesheet" type="text/css" href="css/assignmentStyle.css">
<?php echo $myArray; ?>
</head>
<body>
	
	<h1>This assignment name is PHP Basics</h1>
	<h2>My name is <?php echo $yourName; ?></h2>
	<table>
		<tr>
			<th>Varible Name</th>
			<th>Varible Value</th>
		</tr>
		<tr>
			<td>$number1</td>
			<td><?php echo $number1; ?></td>
		</tr>
		<tr>
			<td>$number2</td>
			<td><?php echo $number2; ?></td>
		</tr>
		<tr>
			<td>$total</td>
			<td><?php echo $total; ?></td>
		</tr>
	</table>
	
	<h2>This is the PHP output of the Javascript Array created in PHP</h2>
	<script>
		for (var i = 0, len = cars.length; i < len; i++) {
			document.write(cars[i]+'</br>')
			}
	</script>
	
	<?php include "include/footer.php" ?>
	<!-- <footer>Click <a href="./wdv341.php">Here</a> to return to the PHP Homework Page</footer> -->

</body>
</html> 