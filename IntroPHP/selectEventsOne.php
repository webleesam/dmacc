<?php

$name="";

$nameError="";

$validForm=false;

$myTable = "";

function validateName()
{
global $name, $nameError, $validForm;    // by using the keyword global these variables referance the varibles outside this function. otherwise these would have been created local variables only for this function
// if($name=="")
if(empty($name))  
{
//echo "Name is Empty<br>";
  $validForm = false;         
  $nameError = "Name is in Error, Name cannot be blank"; 
} 
}


if (isset($_POST["submit"])){


  //echo "Form has been submitted<br>";
  $name = ($_POST["name"]);

  $validForm=true;
  validateName();

} // end of else statement for is set
else {

  //echo "Form has NOT been submitted<br>";

} // End of Else for isset


if ($validForm==true) {
  //echo "Form is Valid<br>";

  $name = ($_POST["name"]);



// connect to database when form is valid - check if local or live (on WebLeeSam.com)
  $ip = $_SERVER['SERVER_ADDR'];

  //echo $ip . "<br>";
  switch ($ip)
  {
    case "104.168.167.168" : 
    //echo "live<br>";
    require "dataBaseConnect.web.php";
    break;

    case "173.17.96.138" : 
    //echo "not live<br>";
    require "dataBaseConnect.local.php";
    break;

    case "192.168.1.20" : 
    //echo "Home Server<br>";
    require "dataBaseConnect.local.php";
    break;

    case "::1" : 
    //echo "localhost<br>";
    require "dataBaseConnect.local.php";
    break;

    default : 
    //echo "There is no match";
    break;
  }


//$stmt->execute(); //or die("Program Killed 03");
  $sql = "SELECT student_id, student_name, student_address, student_email FROM wdv341_student WHERE student_name like '%$name%'";
  $record = $con->query($sql)or die("Program Killed 01");
echo $record->num_rows."<br>";
  
  if ($record->num_rows > 0) {
// output data of each row
    $i = 0;
    while($row = $record->fetch_assoc()) {

      $myData[$i] = $myRowData = array('ID' => $row['student_id'], 'NAME' => $row['student_name'], 'ADDRESS' => $row['student_address'], 'EMAIL' => $row['student_email'] );
//echo $myRowData["NAME"]. "<br>";
      $myTable .= "<tr>";
      $myTable .= "<td>".$myData[$i]['ID']."</td>";
      $myTable .= "<td>".$myData[$i]['NAME']."</td>";
      $myTable .= "<td>".$myData[$i]['ADDRESS']."</td>";
      $myTable .= "<td>".$myData[$i]['EMAIL']."</td>";
      $myTable .= "</tr>";
    }
  } else {
    $myTable .= "<tr>";
    $myTable .= "<td colspan='4'>";
    $myTable .= "0 results<br>";
    $myTable .= "</td>";
    $myTable .= "</tr>";
  }

//printf("Error: %s.\n", $stmt->error. "<br>");


//echo "Records returned successfully<br>";

  $record->close();
  $con->close();



// echo "<a href='selectEventsOne.php'>Vist page again</a><br>";

} else {
//echo "Form is NOT Valid<br>";

} // End of Else for Valid Form
?>


<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>WDV341 Intro PHP - Form Validation Example</title>
  <link rel="stylesheet" type="text/css" href="css/assignmentStyle.css">
  <style>

    #orderArea  {
      width:900px;
      background-color:#CF9;
    }

    .error  {
      color:red;
      font-style:italic;  
    }
    table {
      width: auto;
    }

  </style>
</head>

<body>
  <a href="selectEventsOne.php">Vist page again</a> 
  <h1>WDV341 Intro PHP</h1>
  <h2>Form Validation Assignment
  </h2>



  <div id="orderArea">

    <form id="form1" name="form1" method="post" action="selectEventsOne.php">
      <!-- <form id="form1" name="form1" method="post" action="?"> -->
      <h3>Customer Registration Form</h3>
      <table width="787" border="0">
        <tr>
          <td width="117">Name:</td>
          <td width="246"><input type="text" name="name" id="name" size="40" value="<?php echo $name; ?>"/></td>
          <td width="410" class="error"><?php echo "$nameError"; ?></td>
        </tr>

      </table>

      <p>
        <input type="submit" name="submit" id="submit" value="Submit" />
        <input type="reset" name="reset" id="reset" value="Clear Form" />
      </p>

    </form>
  </div> <!-- end of order area -->

  <table border="1xp">
    <tr>
      <th>ID</th>
      <th>NAME</th>
      <th>ADDRESS</th>
      <th>EMAIL</th>
    </tr>
    <?php echo $myTable; ?>

  </table>

  <?php include "include/footer.php" ?>

<!-- Used to go back one page or return to root folder -->
<footer>
  <a href='eventsForm.php'>Visit Page Again</a><br>
  <a href='#' onClick='history.go(-1);return true;'>Go Back</a><br>
  <a href='wdv341.php'>Main Homework Page</a><br>
  <a href='./'>Return to Root Folder</a><br>
</footer>
  
</body>
</html>