<!DOCTYPE html>
<html>
	<head>
		<title>Assignment: Git Terminology</title>
		<style>

		</style>
		<link rel="stylesheet" type="text/css" href="css/assignmentStyle.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
		<script>
			$(document).ready(function(){

				
			});

		
		</script>
		
	</head>
	
	<body >
	<div id="container">
		<h1>Assignment: Git Terminology</h1>
		<h2 id="returnHome"><a href="javascript:history.back()">Click Here toReturn to Homework Page</a></h2>
	
		<h3>Review and research the following objects.  Define each of the following objects.  Your definition 
		should be at least two paragraph for each object.</h3>
		
		<ol>
			<li>Version Control Software</li>
				<ul>
					<li>"Also known as revision control or source control software. used to manage changes of large websites by developers"
					-- Credit--<a href="https://en.wikipedia.org/wiki/Version_control">https://en.wikipedia.org/wiki/Version_control
					</a></li>
				</ul>
			<li>Git</li>
				<ul>
					<li>"Version control system that is used for software development"
					-- Credit--<a href="https://en.wikipedia.org/wiki/Git">https://en.wikipedia.org/wiki/Git
					</a></li>
				</ul>
			<li>repository</li>
				<ul>
					<li>"A GIT repository contains commit objects and referances to commit objects"
					-- Credit--<a href="https://www.sbf5.com/~cduan/technical/git/git-1.shtml">https://www.sbf5.com/~cduan/technical/git/git-1.shtml
					</a></li>
				</ul>
			<li>stage</li>
				<ul>
					<li>"staging the files is done before the commit. it gets the files ready for the commit"
					-- Credit--<a href="http://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git">http://softwareengineering.stackexchange.com/questions/119782/what-does-stage-mean-in-git
					</a></li>
				</ul>
			<li>commit</li>
				<ul>
					<li>"commit objects are the files reflecting the state of the project at a given point"
					-- Credit--<a href="https://www.sbf5.com/~cduan/technical/git/git-1.shtml">https://www.sbf5.com/~cduan/technical/git/git-1.shtml
					</a></li>
				</ul>
			<li>push</li>
				<ul>
					<li>"push moves the files locally from the machine to the repository"
					-- Credit--<a href="">
					</a></li>
				</ul>
			<li>pull</li>
				<ul>
					<li>"pull moves the files from the repository to the local machine"
					-- Credit--<a href="">
					</a></li>
				</ul>
			<li>revert</li>
				<ul>
					<li>"Similar to undo. It will undo the commit snapshot. The git revert command undoes a committed snapshot. But, instead of removing the commit from the project history, it figures out how to undo the changes introduced by the commit and appends a new commit with the resulting content. This prevents Git from losing history, which is important for the integrity of your revision history and for reliable collaboration."
					-- Credit--<a href="https://www.atlassian.com/git/tutorials/undoing-changes/git-checkout">https://www.atlassian.com/git/tutorials/undoing-changes/git-checkout
					</a></li>
				</ul>
			<li>branching</li>
				<ul>
					<li>"allows you to diverg from the main production code to make changes without worry of meesing up the current code."
					-- Credit--<a href="">
					</a></li>
				</ul>
			<li>merging</li>
				<ul>
					<li>"is bringing a branched code back into the main line code."
					-- Credit--<a href="https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell">https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell
					</a></li>
				</ul>
			<li>change history</li>
				<ul>
					<li>"there records of all changes. push and pulls"
					-- Credit--<a href="">
					</a></li>
				</ul>
			<li>clone</li>
				<ul>
					<li>"copies a repository to your local machine. used when the project is not on your machine. It could almost be called the first pull"
					-- Credit--<a href="">
					</a></li>
				</ul>
		</ol>
	</div> <!--End of container-->
	<footer>Click <a href="./wdv341.php">Here</a> to return to the PHP Homework Page</footer>	
	</body>
</html> 