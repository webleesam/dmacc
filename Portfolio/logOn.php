<?php
session_start();
if (!isset($_SESSION['validUser'])) {
  $_SESSION['validUser'] = "waiting";
}  

if ($_SESSION['validUser'] == "yes") {  
  $logOnMessage = "Welcome Back!<br>"; 
} else {
  $logOnMessage = "You need to log on<br>"; 
  $name = "";
  $nameError="";
  $password = "";
  $passwordError="";

  // 2nd set form valid to false. assume form is false, let validation functions change it to true
  $validForm=false;

  // 3rd create validattion functions. must pass (define) variables globally
  require "php/validate.php";

  if (isset($_POST["submit"])) {
    $name = ($_POST["name"]);
    $password = ($_POST["password"]);
    // 2nd set form valid to true. assume form is true, let validation functions change it to false
    $validForm=true;
    validateName($name);
    validatePassword($password);
	  if ($validForm==true) {
      require "php/dbConnectLogOn.php";
		  

		  $statement = "SELECT event_user_user,event_user_password FROM event_user WHERE event_user_user = ? AND event_user_password = ?";        
		  $query = $con->prepare($statement) or die("Program Killed 01");  
		  $query->bind_param("ss",$name,$password)  or die("Program Killed 02"); 
		  $query->execute()  or die("Program Killed 03"); 
		  $query->bind_result($name,$password) or die("Program Killed 04"); ;
		  $query->store_result();
		  $query->fetch();

		  //If this is a valid user there should be ONE row only
		  if ($query->num_rows == 1 ) {
		    $_SESSION['validUser'] = "yes";       //this is a valid user so set your SESSION variable
        $_SESSION['userName'] = $name;
        $logOnMessage = "Welcome Back! $name <br>";
		    
		    //Valid User can do the following things:
		  } else {
		  	
		  	$_SESSION['validUser'] = "no";          
        $logOnMessage = "Sorry, there was a problem with your username or password. Please try again.<br>";
		  }
		  $query->close();
		  $con->close();
    }	  
	} 
} // End of Else for Valid User in controller
  ?>

<!-- VIEW -->
<!-- ****************************** -->

	<!DOCTYPE html>
	<html>
	<head>
	<title>logOn.php</title>
<!-- Core -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!-- ^Core -->

	<link rel="stylesheet" type="text/css" href="css/style.css">

	</head>

	<body>

  <!-- NAVAGATION BAR -->
   <!-- *********************************** -->
   <?php include "php/navigationMenu.php" ?> 

   <!-- *************************** -->
  <!-- END OF NAV BAR -->

  <?php include "php/validUserCheckMainMenu.php" ?>

  </body>
</html>