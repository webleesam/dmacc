      <div class="form-group">
        <label>Name: </label>
        <input class="form-control" type="text" id="name" name="name" value=<?php echo $name; ?> >
        <div class="error"><?php echo $nameError ?></div>
      </div>
      <div>
        <label>Password: </label>
        <!-- change type to password to 'star' the input field -->
        <input class="form-control" type="text" id="password" name="password" value=<?php echo $password; ?> >
        <span class="error"><?php echo $passwordError ?></span>
      </div>
      <input type="submit" class = "btn btn-success" name="submit" id="submit" value="Submit">
      <input type="reset" class = "btn btn-warning" name="reset" id="reset" value="Reset">
      <input type="button" class = "btn btn-danger" name="clearForm" id="clearForm" value="Clear Form">