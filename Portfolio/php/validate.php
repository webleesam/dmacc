<?php
    function validateBlank($x) {
    global $validForm;
    $xError = ""; 
    if(empty($x)) {
      $validForm = false;         
      $xError .= "Input Field cannot be blank!"; 
      return $xError; 
    } 
  }

  function validateSpecial($x) {
    global $validForm;
    $xError = "";
    if (preg_match("([\'\/~`\!@#\$\"\\%\^&\*\(\)_\-\+=\{\}\[\]\|;:<\>,\.\?])",$x)) {  //From http://stackoverflow.com/questions/3937569/preg-match-php-special-characters
      $validForm = false;
      $xError .= "Input Field contains an illegal character!"; 
      return $xError;
    }
  }

  function validatePassword ($x) {
    global $passwordError;
    $passwordError .= validateBlank($x);
    $passwordError .= validateSpecial($x);
  }
 
  
  function validateNonNumber($x) {
      global $validForm;
      $xError = "";
      if (!empty($x)) { 
        if (!preg_match("/^[a-zA-Z\s]+$/",$x)) {  echo "not match<br>";
          $validForm = false;
            $xError .= "Input Field contains Letters!"; 
            return $xError;
         } 
     }  
  }
  
  function validateYesNumber($x) {
      global $validForm;
      $xError = "";
      if (!empty($x)) { 
        if (preg_match("/^[a-zA-Z\s]+$/",$x)) {
            $validForm = false;
            $xError .= "Input Field contains Number!"; 
            return $xError;
        } 
    }  
  }
  
  function validateName ($x) {
    global $nameError;
    $nameError .= validateBlank($x);
    $nameError .= validateSpecial($x);
    $nameError .= validateNonNumber($x);
  }

  function validateLogOnName ($x) {
    global $logOnNameError;
    $nameError .= validateBlank($x);
    $nameError .= validateSpecial($x);
    $nameError .= validateNonNumber($x);
  }
  
  function validateDate ($x) {
    global $dateError;
    $dateError .= validateBlank($x);
  }
  
  function validateJobNumber ($x) {
    global $jobnumberError;
    $jobnumberError .= validateBlank($x);
    $jobnumberError .= validateSpecial($x);
    $jobnumberError .= validateYesNumber($x);
  }
  
  function validateJobName ($x) {
    global $jobnameError;
    $jobnameError .= validateBlank($x);
    $jobnameError .= validateSpecial($x);
    $jobnameError .= validateNonNumber($x);
  }
  
  function validateRt ($x) {
    global $rtError;
    $rtError .= validateSpecial($x);
    $rtError .= validateYesNumber($x);
  }
  
  function validateOt ($x) {
    global $otError;
    $otError .= validateSpecial($x);
    $otError .= validateYesNumber($x);
  }
  
  function validateDt ($x) {
    global $dtError;
    $dtError .= validateSpecial($x);
    $dtError .= validateYesNumber($x);
  }

   ?>