	<div id="container2">
    <?php
    if (!$_SESSION['validUser'] == "yes") {
      ?>
      <h2><?php echo $logOnMessage; ?></h2>
      <?php
    }
    ?>
    <?php
    if ($_SESSION['validUser'] == "yes") {
      ?>
      <h3>Presenters Administrator Options</h3>
      <p><a href="timesheetEntry.php">Input Timesheet Data</a></p>
      <p><a href="timesheetResults.php">Review Timesheet Data</a></p>
      <p><a href="logOut.php">Log Off</a></p> 
      <?php
    } else {
      ?>
      <h1>Log On Form</h1>
      <h2><?php echo $logOnMessage; ?></h2>
      <form id="myForm" name="myForm" method="post" action="logOn.php">
        <?php require "php/logOnForm.php" ?>
      </form>

      <form id="myForm" name="myForm" method="post" action="logOn.php" class='dropdown-menu'>
     
      </form>
      <?php
    } // End of Else for Valid User in view
    ?>
    <footer>
      <!-- Used to go back one page or return to root folder -->
      <a href='logOn.php'>Visit Page Again</a><br>
      <a href='#' onClick='history.go(-1);return true;'>Go Back</a><br>
      <a href='../wdv341.php'>Main Homework Page</a><br>
      <a href='../'>Return to Root Folder</a><br>
    </footer>

  </div> <!-- end of container -->