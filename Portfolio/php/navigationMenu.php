<!-- NAVAGATION BAR -->
 <!-- *********************************** -->

  <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
      <span class="sr-only">Toggle navigation</span>
      <!-- <span class="icon-bar"></span> -->
      <span class="glyphicon glyphicon-option-horizontal white"></span>
      </button>
      <a class="navbar-brand" href="#">Logo</a>
      
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="myNavbar">
          <ul class="nav navbar-nav">
          <?php if(isset($_SESSION['userName'])) {echo "<li><a href='timesheetEntry.php'>Timesheet Entry</a></li><li><a href='timesheetResults.php'>Timesheet Review</a></li>";} ?>
        </ul>
        <ul class="nav navbar-nav navbar-right">
          <li class="<?php if(empty($nameError || $passwordError)) {echo 'dropdown';} else {echo 'dropdown open';} ; ?>">
            <?php if(isset($_SESSION['userName'])) {echo "<a href='logOut.php'>Log Off</a>"; } else {echo "<a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Logon<span class='caret'></span></a>"; } ?>
            <form id="myForm" name="myForm" method="post" action="logOn.php" class='dropdown-menu'>
            <?php require "php/logOnForm.php" ?>
            </form>
          </li>                                  
      </ul>
    </div>
  <!-- /.navbar-collapse -->
  </div>
  <!-- /.container -->
  </nav>
  <br>
  <br>
  <br>
  <div class="container container-black">
  <br>
 <!-- *************************** -->