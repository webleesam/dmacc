<?php
session_start();
if ($_SESSION['validUser'] == "yes") {
	$logOnName = $_SESSION['userName'];
	$logOnMessage = "Welcome Back! $logOnName<br>";

	// Self Posting Page
	// 1st add (define) variables
	
	$name = "test1";
	$nameError="test2";
	$date = "Click here to select date";
	$dateError="";
	$jobnumber = "Enter in a Job Number";
	$jobnumberError="";
	$jobname = "Enter in a Job Name";
	$jobnameError="";
	$rt = 0;
	$rtError="";
	$ot = 0;
	$otError="";
	$dt = 0;
	$dtError="";

	$mySentStatus="TEST TEXT HERE TO BE REPLACED";

	// 2nd set form valid to false. assume form is false, let validation functions 	change it to true
	
	$validForm=false;
	//if ($validForm) {echo "form valid:  <br>";}
	

	// 3rd create validation functions. must pass (define) variables globally
	require "php/validate.php";
	
	if (isset($_POST["submit"])){
		$date = ($_POST["date"]);
		if ($date == "Click here to select date") {
			$date = "";
		}
		$name = ($_POST["name"]);
		$jobnumber = ($_POST["jobnumber"]);
		if ($jobnumber == "Enter in a Job Number") {
			$jobnumber = "";
		}
		$jobname = ($_POST["jobname"]);
		if ($jobname == "Enter in a Job Name") {
			$jobname = "";
		}
		$rt = ($_POST["rt"]);
		$ot = ($_POST["ot"]);
		$dt = ($_POST["dt"]);

		// 2nd set form valid to true. assume form is true, let validation functions change 	it to false
		$validForm=true;
		validateName ($name);
		validateDate ($date);
		validateJobNumber ($jobnumber);
		validateJobName ($jobname);
		validateRt ($rt);
		validateOt ($ot);
		validateDt ($dt);
	} 
	
	if ($validForm==true) {
		// connect to database when form is valid - check if local or live (on 	WebLeeSam.com)
		$ip = $_SERVER['SERVER_ADDR'];
	
		switch ($ip) {
			case "104.168.167.168" : 
			//require "dataBaseConnect.web.php";
			require "dataBaseConnect.web.timesheet.php";
			break;
	
			case "173.17.96.138" : 
			echo "not live<br>";
			//require "dataBaseConnect.local.php";
			require "dataBaseConnect.local.php";
			break;
	
			case "192.168.1.20" : 
			echo "Home Server<br>";
			//require "dataBaseConnect.local.php";
			require "dataBaseConnect.local.timesheet.php";
			//require "db.inc.php";
			//require "func.inc.php";
			//connect();
			break;
	
			case "::1" : 
			echo "localhost<br>";
			require "dataBaseConnect.local.php";
			break;
	
			default : 
			echo "There is no match";
			break;
		}
	
		$stmt = $con->prepare("INSERT INTO weekly (name, date, jobname, jobnumber, rt, ot, dt) VALUES (?, ?, ?, ?, ?, ?, ?)") or die("Program Killed 02");
	
		$stmt->bind_param("sssiiii", $name, $date, $jobname, $jobnumber, $rt, $ot, $dt) 	or die("Program Killed 02.1");
	
		$stmt->execute();
	
		$logOnMessage = "New records created successfully<br>";
	
		$stmt->close();
		$con->close();
		

	  	?>
	  	<form id="emailButton" action="emailTimesheet.php" method="post">
	  	<p>Once you have enter in your time press this <input type="submit" name="emailSubmit" value="Email Button" /> to notify your manager to review your timesheet</p>
	  	</form>
	  	<?php



	} else { // valid form
	
		?>

		<!-- VIEW -->
		<!-- ****************************** -->	
		<!DOCTYPE html>
		<html>
		<head>
	    <title>Time Sheet Entry Form</title>
	    <!-- Core -->
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<!-- ^Core -->
	    <link rel="stylesheet" type="text/css" href="css/style.css">
	    <link rel="stylesheet" type="text/css" href="css/timesheetEntry.css">


		<!-- jQuery Datepicker Widget   -->
				<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/	smoothness/jquery-ui.css">
				<script src="//code.jquery.com/jquery-1.10.2.js"></script>
				<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
				<script>
				$(function() {
				$( "#datepicker" ).datepicker( {
					dateFormat: 'yy-mm-dd',
					} );
				});
				</script>
		<script src="js/script.js"></script>	
		<script type="text/javascript">
			
    	</script>	
		</head>
		<body>
		<!-- NAVAGATION BAR -->
		 <!-- *********************************** -->
		 <?php include "php/navigationMenu.php" ?> 

		 <!-- *************************** -->
		<!-- END OF NAV BAR -->
			<div id="container">
			<h1>Scope of Project</h1>
			<h2><?php echo $logOnMessage; ?></h2>
			<p>Employee timesheet. Name input field ulilizes jQuery and AJAX to call all 	available names from the database. Type in a letter to filter the list. Type 	in a new name to add a new name to the database.</p>
			
				<div id="myForm">
					<form id="entryForm" action="timesheetEntry.php" method="post">
				        <label>Name: </label>
				        <div>
	    					<input id="name" name="name" onClick="Clear(); autoComplete()" autocomplete="off" value="<?php echo $name; ?>">
				        	<div class="error"><?php echo $nameError ?></div>
				        	<div class="resultField" id="suggest"></div>
				        </div>
					        	
						<div>
							<label>Date:</label>
							<input class="form-control" type="text" id="datepicker" name="date" value="<?php echo $date; ?>">
							<span class="error"><?php echo $dateError; ?></span>
						</div>
						<div>
							<label>Job Number:</label>
							<input class="form-control" type="text" name="jobnumber" onClick="ClearNumber();" id="jobnumber" size="25" value="<?php echo $jobnumber; ?>" />
							<span class="error"><?php echo $jobnumberError; ?></span>
						</div>
						<div>
							<lable>Job Name:</lable>
							<input class="form-control" type="text" name="jobname" onClick=" ClearName();" id="jobname" size="25" value="<?php echo $jobname; ?>" />
							<span class="error"><?php echo $jobnameError; ?></span>
						</div>
						<div>
							<lable>RT:</lable><input class="form-control" type="text" name="rt" size="3" value="<?php echo $rt; ?>" /><span class="error"><?php echo $rtError; ?></span>
						</div>
						<div>
							<lable>OT:</lable><input class="form-control" type="text" name="ot" size="3" value="<?php echo $ot; ?>" /><span class="error"><?php echo $otError; ?></span>
						</div>
						<div>
							<lable>DT:</lable><input class="form-control" type="text" name="dt" size="3" value="<?php echo $dt; ?>" /><span class="error"><?php echo $dtError; ?></span>
						</div>
						<input type="submit" name="submit" value="Submit" />
						<input type="reset" name="reset" value="Reset" />
					</form>

					<form id="emailButton" action="emailTimesheet.php" method="post">
					<p>Once you have enter in your time press this <input type="submit" name="emailSubmit" value="Email Button" /> to notify your manager to review your timesheet</p>
					</form>



				</div> <!-- end of myForm -->
				<hr>       <!--    Retrieve            -->
		<?php
	}	
} else {
	header('Location: logOn.php');
}

?>	
	 <footer>
    	<a href='timesheetEntry.php'>Visit Page Again</a><br>
    	<a href='logOn.php'>Log On</a><br>
    	<a href='logOut.php'>Log Out</a><br>
    	<a href='#' onClick='history.go(-1);return true;'>Go Back</a><br>
    	<a href='../wdv341.php'>Main Homework Page</a><br>
    	<a href='../'>Return to Root Folder</a><br>
  	</footer>
  	</div> <!-- end of container -->	
	</body>
	</html> 